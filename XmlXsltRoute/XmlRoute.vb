Imports System.Xml
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration.ConfigurationSettings
Imports System.Web.Mail
Imports System.Text
Imports WbemScripting

Public Class XmlRoute

    Private WithEvents Timer1 As System.Timers.Timer = New System.Timers.Timer()
    Private WithEvents FileSystemWatcher1 As FileSystemWatcher = New FileSystemWatcher()

    Private WithEvents WatchTimer As System.Timers.Timer = New System.Timers.Timer()
    Private WithEvents DelayTimer As System.Timers.Timer = New System.Timers.Timer()

    Private _mutex As Threading.Mutex

    Const TEXT_INIT As String = "NTB_XmlXsltRoute 'Class XmlRoute' Init finished"

    Private Const OK = 1
    Private Const ERR_XML = 2
    Private Const ERR_DELETE = 3

    Enum DoneStatus
        Delete = 1
        Move = 2
        CopyDelete = 3
        Leave = 4
    End Enum

    Enum InteruptType
        TIMER = 1
        NTFS = 2
        NtfsTimer = 3
    End Enum

    'Win API function Sleep
    Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

    'Old deleted: 20.05.2003 RoV
    'Public htXsltProcs As Hashtable
    'Private htXsltProcs As Hashtable
    'Private strXsltPath As String
    'Private htArgumentLists As Hashtable = New Hashtable()
    'Private dsXsltJobs As DataSet
    ' Private objMain As FormTest
    'Public objXchgReplace As XchgReplace

    'Public class variabler: 
    Public htDistKodeSatAviser As Hashtable
    Public bBusy As Boolean
    Public intGroupListID As Integer

    'Private
    Private objSniff As New SniffSearch()
    Private logFilePath As String
    Private bEnableRaisingEvents As Boolean
    Private bTimerEnabled As Boolean
    Private bEnableRaisingEventsPause As Boolean
    Private bTimerEnabledPause As Boolean

    'Internal class variabler: 
    Private htTempFiles As Hashtable = New Hashtable()
    Private htMailCache As Hashtable = New Hashtable()
    Private htErrorStates As Hashtable = New Hashtable()

    Private dsCustomerJob As DataSet
    Private dsCustomer As DataSet
    Private dsCustomerDistKode As DataSet
    Private dsWatchFolder As DataSet
    Private dsDelayFolder As DataSet

    Private errorSMSNumber As String
    Private errorMailAddress As String

    Private strInputPath As String
    Private strInputFilter As String
    Private strDonePath As String

    Private strDelayPath As String
    Private strDefOutPath As String

    Private strErrorPath As String
    Private databaseOfflinePath As String

    Private intDoneStatusID As Integer
    Private intSleepSec As Integer
    Private intPollingInterval As Integer
    Private intInteruptType As InteruptType

    Private isXmlInput As Boolean

    'Default constructor
    Public Sub New()
    End Sub

    'Constructor
    Public Sub New(ByVal cn As OleDbConnection, ByVal rowGroupList As DataRow, ByVal strLogFilePath As String, ByVal strDatabaseOfflinePath As String) ', ByVal xsltPath As String)

        intGroupListID = rowGroupList("GroupListID") & ""

        _mutex = New Threading.Mutex(False, "XMLROUTE_" & intGroupListID & "_LOGFILE")

        logFilePath = strLogFilePath & "\GroupList" & intGroupListID
        databaseOfflinePath = strDatabaseOfflinePath
        strDonePath = rowGroupList("DonePath") & ""
        intDoneStatusID = rowGroupList("DoneStatusID") & ""
        intSleepSec = rowGroupList("SleepInterval") & ""
        strInputPath = rowGroupList("InputPath") & ""
        intPollingInterval = rowGroupList("PollingInterval") & ""
        intInteruptType = rowGroupList("InteruptType") & ""
        strErrorPath = rowGroupList("ErrorPath") & ""
        isXmlInput = rowGroupList("IsXmlInput") & ""
        strInputFilter = rowGroupList("InputFilter") & ""
        errorSMSNumber = rowGroupList("ErrorSMSNumber") & ""
        errorMailAddress = rowGroupList("ErrorEmail") & ""

        'Folders folder for holded messages
        strDefOutPath = rowGroupList("DefCustomerRootPath") & ""
        strDelayPath = rowGroupList("DelayPath") & ""

        FillDataSets(cn)
        FillHashtables()

        CreateOutDirectories()

#If Not Debug Then
        CreateFTPVirtualDirs()
        CreateUsers()
#End If

        'Surveillance
        DelayTimer.Interval = intPollingInterval * 1000
        DelayTimer.Enabled = True

        'Surveillance
        WatchTimer.Interval = watchInitialDelay
        WatchTimer.Enabled = True

        'File handling
        Timer1.Interval = intPollingInterval
        FileSystemWatcher1.Path = strInputPath
        FileSystemWatcher1.Filter = strInputFilter

        Select Case intInteruptType
            Case InteruptType.TIMER
                bTimerEnabled = True
            Case InteruptType.NTFS
                bEnableRaisingEvents = True
            Case InteruptType.NtfsTimer
                bTimerEnabled = True
                bEnableRaisingEvents = True
        End Select
        WriteLog(logFilePath, TEXT_INIT)
    End Sub

    Private Sub WatchTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles WatchTimer.Elapsed
        WatchTimer.Enabled = False
        _mutex.WaitOne()

        Try
            If SMTPRunning() Then WatchFolders()
        Catch ex As Exception
            WriteErr(logFilePath, "Feil ved mappeoverv�kning.", ex)
        End Try

        _mutex.ReleaseMutex()
        WatchTimer.Interval = intPollingInterval * 1000
        WatchTimer.Enabled = True
    End Sub

    Private Sub DelayTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles DelayTimer.Elapsed
        DelayTimer.Enabled = False
        _mutex.WaitOne()

        Try
            MoveDelayed()
        Catch ex As Exception
            WriteErr(logFilePath, "Feil ved flytting av hold-filer.", ex)
        End Try

        _mutex.ReleaseMutex()
        DelayTimer.Interval = intPollingInterval * 1000
        DelayTimer.Enabled = True
    End Sub

    Private Sub Timer1_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer1.Elapsed
        bBusy = True
        Timer1.Enabled = False
        FileSystemWatcher1.EnableRaisingEvents = False
        _mutex.WaitOne()

#If Debug Then
        Beep()
#End If

        Timer1.Interval = intPollingInterval * 1000
        DoAllInputFiles()

        _mutex.ReleaseMutex()
        Timer1.Enabled = bTimerEnabledPause
        FileSystemWatcher1.EnableRaisingEvents = bEnableRaisingEventsPause
        bBusy = False
    End Sub

    Private Sub FileSystemWatcher1_Created(ByVal sender As System.Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileSystemWatcher1.Created
        bBusy = True
        FileSystemWatcher1.EnableRaisingEvents = False
        Timer1.Enabled = False
        _mutex.WaitOne()

#If Debug Then
        Beep()
        Debug.WriteLine(Now & ": Created: " & e.Name & ", ChangeType: " & e.ChangeType & ", NotifyFilter: " & FileSystemWatcher1.NotifyFilter)
#End If

        'Wait some seconds for last inputfile file to be written by external program, before reading
        Sleep(intSleepSec * 1000)

        'DoOneInputFile(e.FullPath)
        'Do new files which might have appeared during DoOneInputFile
        DoAllInputFiles()

        _mutex.ReleaseMutex()
        FileSystemWatcher1.EnableRaisingEvents = bEnableRaisingEventsPause
        Timer1.Enabled = bTimerEnabledPause
        bBusy = False
    End Sub

    Public Sub Start()
        bTimerEnabledPause = bTimerEnabled
        Timer1.Enabled = bTimerEnabled
        bEnableRaisingEventsPause = bEnableRaisingEvents
        FileSystemWatcher1.EnableRaisingEvents = bEnableRaisingEvents

        'Do any files waiting in Input Path when starting program
        'Must do this for DeleteFIleSync to work properly - Nope fixed Deletefilesync :)
        'DoAllInputFiles()

        Timer1.Interval = 2000
        Timer1.Enabled = True
    End Sub

    Public Sub Pause()
        bTimerEnabledPause = False
        Timer1.Enabled = False
        bEnableRaisingEventsPause = False
        FileSystemWatcher1.EnableRaisingEvents = False
    End Sub

    Private Sub DoAllInputFiles() 'ByRef rowGroupList As DataRow, ByRef intCount As Integer)
        Dim arrFileList() As String
        Dim strInputFile As String
        Static intCount As Long

        'Read all files in folder
#If Not Debug Then
        Try
#End If
        arrFileList = Directory.GetFiles(strInputPath, strInputFilter)
#If Not Debug Then
        Catch e As Exception
            WriteErr(logFilePath, "Feil input directory: " & strInputPath, e)
            Exit Sub
        End Try
#End If
        'Wait some seconds for last inputfile file to be written by external program, before reading
        Sleep(intSleepSec * 1000)
        For Each strInputFile In arrFileList
            DoOneInputFile(strInputFile)
            intCount += 1
            Application.DoEvents()
        Next
    End Sub

    Private Sub DoOneInputFile(ByVal strInputFile As String)
#If Not Debug Then
        Try
#End If
        WriteLogNoDate(logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
        WriteLog(logFilePath, "Start: " & strInputFile)
        WriteLogNoDate(logFilePath, "---------------------------------------------------------------------------------------------------------------------------")

        RouteFile(strInputFile, strDonePath, intDoneStatusID)

#If Not Debug Then
        Catch e As Exception
            Try
                WriteErr(logFilePath, "Feilet: " & strInputFile, e)
                Dim strOutPutFile As String = strErrorPath & "\" & Path.GetFileName(strInputFile)
                File.Copy(strInputFile, strOutPutFile)
                File.Delete(strInputFile)
            Catch
            End Try
        End Try
#End If
    End Sub

    Private Function RouteFile(ByVal strFilename As String, ByVal strDonePath As String, ByVal intDoneStatusID As DoneStatus) As Integer
        Dim xmlDoc As XmlDocument = New XmlDocument()
        'Dim xpathDoc As XPath.XPathDocument
        'Dim bCheckOk As Boolean
        Dim rowCustomer As DataRow
        Dim strTempFile As String
        Dim strDoneFile As String
        Dim i As Integer
        Dim strDisKode As String

        If isXmlInput Then

            'Try
            xmlDoc.Load(strFilename)
            objSniff.SetXmlText(xmlDoc)
            'Catch
            'Return ERR_XML
            'End Try

            strDisKode = GetArticleDistKode(xmlDoc)

            For Each rowCustomer In dsCustomer.Tables(0).Rows
                If CheckForCopy(xmlDoc, rowCustomer, strDisKode) Then
                    CopyTrans(strFilename, rowCustomer)
                Else
                    WriteLogNoDate(logFilePath, "                     Ikke til: " & rowCustomer("CustomerName") & ", XpathDef: " & rowCustomer("XpathName"))
                End If
            Next
        Else
            'For text files then no Check before copy, just copy all files to all customers
            For Each rowCustomer In dsCustomer.Tables(0).Rows
                CopyTrans(strFilename, rowCustomer)
                WriteLog(logFilePath, "Kopiert Til: " & rowCustomer("CustomerName"))
            Next
        End If

        'Delete Temp Files and cache
        If deleteTempFiles Then
            For Each strTempFile In htTempFiles.Values
                File.Delete(strTempFile)
            Next
        End If
        htTempFiles.Clear()
        htMailCache.Clear()

        'Try
        Dim strStatus As String
        strDoneFile = strDonePath & Path.DirectorySeparatorChar & Path.GetFileName(strFilename)

        'Create datesub
        strDoneFile = MakeSubDirDate(strDoneFile, File.GetLastWriteTime(strFilename))

        Select Case intDoneStatusID
            Case DoneStatus.CopyDelete
                File.Copy(strFilename, strDoneFile, True)
                File.Delete(strFilename)
                strStatus = "CopyDeleted to: " & strDoneFile
            Case DoneStatus.Move
                File.Move(strFilename, strDoneFile)
                strStatus = "Moved to: " & strDoneFile
            Case DoneStatus.Delete
                File.Delete(strFilename)
                strStatus = "Deleted: " & strFilename
            Case DoneStatus.Leave
                strStatus = "Not Deleted: " & strFilename
                'do nothing
        End Select
        WriteLogNoDate(logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
        WriteLog(logFilePath, "Routing finished: " & strStatus)

        'Catch e As Exception

        '    WriteErr(logFilePath, "Deleting failed: ", e)
        '    Return ERR_DELETE

        'End Try

        Return OK

    End Function

    'Add local user for FTP fetch
    Private Sub CreateUsers()

        Dim row As DataRow

        Dim computername As String = System.Environment.MachineName
        Dim objComputer As Object
        Dim objGroup1 As Object
        Dim objGroup2 As Object
        Dim objUser As Object

        objComputer = GetObject("WinNT://" & computername)
        objGroup1 = GetObject("WinNT://" & computername & "/Users")
        objGroup2 = GetObject("WinNT://" & computername & "/FTP Users")


        For Each row In dsCustomer.Tables(0).Rows

            Dim ftpGet As Integer
            Dim name As String
            Dim password As String

            Try
                ftpGet = row("FtpTypeID")
                name = row("Username")
                password = row("Password")
            Catch
            End Try


            If ftpGet = 1 And name <> "" Then
                'Execute shell VBScript
                Try
                    objUser = objComputer.GetObject("User", name)
                Catch
                    objUser = objComputer.Create("User", name)
                End Try

                Try
                    objUser.SetPassword(password)
                    objUser.SetInfo()

                    Try
                        objGroup1.Remove("WinNT://" & computername & "/" & name)
                        objUser.SetInfo()
                    Catch
                    End Try

                    Try
                        objGroup2.Add("WinNT://" & computername & "/" & name)
                        objUser.SetInfo()
                    Catch
                    End Try

                Catch e As Exception
                    WriteErr(logFilePath, "Failed to create user!", e)
                End Try
            End If

        Next

    End Sub

    'Add vdirs for FTP get
    Private Sub CreateFTPVirtualDirs()

        Dim row As DataRow
        Dim ok As Boolean = True

        Dim conn As SWbemLocatorClass = New SWbemLocatorClass()
        Dim prov As SWbemServices
        Dim node, n2, n3 As SWbemObject
        Dim ret As SWbemObjectPath

        Try
            prov = conn.ConnectServer(".", "root/MicrosoftIISv2")
        Catch e As Exception
            WriteErr(logFilePath, "Failed to create FTP virtual dirs!", e)
            ok = False
        End Try

        If ok Then
            For Each row In dsCustomer.Tables(0).Rows

                Dim ftpGet As Integer
                Dim name As String
                Dim folder As String

                Try
                    ftpGet = row("FtpTypeID")
                    name = row("Username")
                    folder = row("DefOutPath")
                Catch
                End Try

                If ftpGet = 1 And name <> "" Then

                    Try
                        n2 = prov.Get("IIsFtpVirtualDirSetting='MSFTPSVC/1/ROOT/" & name & "'")
                    Catch a As Exception
                        n2 = prov.Get("IIsFtpVirtualDirSetting")
                    End Try

                    node = n2.SpawnInstance_()

                    node.Properties_.Item("Name").Value = "MSFTPSVC/1/ROOT/" & name
                    node.Properties_.Item("Path").Value = folder
                    node.Properties_.Item("AccessRead").Value = True
                    node.Properties_.Item("AccessWrite").Value = True

                    node.Put_()

                End If
            Next
        End If

    End Sub

    Private Sub CreateOutDirectories()

        Dim strOutPath As String
        Dim bEmail As Boolean
        Dim delay As Integer

        Dim row As DataRow

        'Make paths
        For Each row In dsCustomerJob.Tables(0).Rows
            strOutPath = row("OutPath")

            bEmail = row("Email")
            If strOutPath <> "" And Not bEmail Then
                Directory.CreateDirectory(strOutPath)
            End If

            delay = row("PushDelay")
            If delay > 0 Then
                'Calculate temp dir for customer
                Directory.CreateDirectory(strOutPath.Replace(strDefOutPath, strDelayPath))
            End If

        Next

        Directory.CreateDirectory(strDonePath)
        Directory.CreateDirectory(strErrorPath)
        Directory.CreateDirectory(strInputPath)
        Directory.CreateDirectory(logFilePath)
    End Sub

    Private Sub FillDataSets(ByRef cn As OleDbConnection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand = New OleDbCommand()

        dataAdapter.SelectCommand = command
        command.Connection = cn

        dsCustomerJob = New DataSet()
        dsCustomer = New DataSet()
        dsCustomerDistKode = New DataSet()
        dsWatchFolder = New DataSet()
        dsDelayFolder = New DataSet()

        FillOneDataSet(dsCustomer, "dsCustomer", "SELECT * FROM GetCustomer WHERE GroupListID=" & intGroupListID, dataAdapter, command, intGroupListID)
        FillOneDataSet(dsCustomerJob, "dsCustomerJob", "SELECT * FROM GetCustomerJob WHERE GroupListID=" & intGroupListID, dataAdapter, command, intGroupListID)
        FillOneDataSet(dsCustomerDistKode, "dsCustomerDistKode", "SELECT * FROM CustomerDistKode", dataAdapter, command, intGroupListID)
        FillOneDataSet(dsWatchFolder, "dsWatchFolder", "SELECT * FROM GetWatchFolders WHERE GroupListID=" & intGroupListID, dataAdapter, command)
        FillOneDataSet(dsDelayFolder, "dsDelayFolder", "SELECT * FROM GetDelayFolders WHERE GroupListID=" & intGroupListID, dataAdapter, command)

        dataAdapter = Nothing
        command = Nothing

    End Sub

    Private Sub FillHashtables()
        Dim row As DataRow
        Dim jobDefID As Integer

        For Each row In dsCustomer.Tables(0).Rows
            Dim strSniffText As String = row("SniffText") & ""
            If strSniffText <> "" Then
                Dim customerID As Integer = row("customerID")
                objSniff.SetRegexCustomer(customerID, strSniffText, logFilePath)
            End If
        Next

        'Dim xsltFile As String
        'htXsltProcs = New Hashtable()

        ''Lag XSLT prosessors for hver XSLT-fil og legg i Hashtable
        'For Each row In dsXsltJobs.Tables(0).Rows
        '    jobDefID = row("JobDefID")
        '    xsltFile = row("XsltFile") & ""
        '    If xsltFile <> "" Then
        '        Dim xsltProc As Xsl.XslTransform = New Xsl.XslTransform()
        '        Try
        '            xsltProc.Load(strXsltPath & "\" & xsltFile)
        '            htXsltProcs.Item(jobDefID) = xsltProc
        '        Catch e As Xsl.XsltException
        '            WriteErr(logFilePath, "Feil i lasting av XSLT file: " & xsltFile, e)
        '        End Try
        '    End If
        'Next

        ''Lag XsltArgumentList for hver Kunde og legg i Hashtable
        'Dim xslParam As Xsl.XsltArgumentList = New Xsl.XsltArgumentList()
        'For Each row In dsCustomerJob.Tables(0).Rows
        '    Dim strParam As String = row("XsltParamsCust") & ""
        '    If strParam <> "" Then
        '        Dim customerID As Integer = row("customerID")
        '        jobDefID = row("jobDefID")

        '        Dim arrParams As String() = Split(strParam, vbCrLf)
        '        For Each strParam In arrParams
        '            Dim strParamValue As String = row(strParam) & ""
        '            xslParam.AddParam(strParam, "", strParamValue)
        '        Next
        '        htArgumentLists.Add(customerID & "-" & jobDefID, xslParam)
        '    End If
        'Next
    End Sub

    Private Function CheckForCopy(ByRef xmlDoc As XmlDocument, ByRef rowCustomer As DataRow, ByVal strDisKode As String) As Boolean
        Dim strXpath As String = rowCustomer("XPath") & ""
        Dim customerId As String

        If strXpath = "" Then
            'Tom xpath gir kopiering uansett
            Return True
        End If

        If IsCustomerDistKode(rowCustomer, strDisKode) Then
            'Hvis meldingen innheolder Distribusjonkode eller avisens kode, kopier
            Return True
        End If

        If IsAvisDistKode(strDisKode) Then
            'Hvis meldingen finnes med en aviskode, ikke kopier
            Return False
        End If

        'Check xpath and sniff if given
        customerId = rowCustomer("CustomerID")
        If CheckXpath(xmlDoc, rowCustomer) And objSniff.Find(customerId) <> 0 Then
            Return True
        End If

        '' At last if not found in other methods try Sniff search:
        'If objSniff.Find(customerId) > 0 Then
        '    Return True
        'End If

        Return False

    End Function

    Private Function CheckXpath(ByRef xmlDoc As XmlDocument, ByRef rowCustomer As DataRow) As Boolean
        Dim nodeList As XmlNodeList
        Dim strCompare As String
        Dim intCompareHit As Integer
        Dim intHits As Integer
        Dim bFound As Boolean

        strCompare = rowCustomer("compare")
        intCompareHit = rowCustomer("hits")
        Dim strXpath As String = rowCustomer("XPath") & ""

        Try
            nodeList = xmlDoc.SelectNodes(strXpath)
            intHits = nodeList.Count
        Catch e As Exception
            WriteErr(logFilePath, "Feil i XPath: " & strXpath, e)
            Return False
        End Try

        bFound = False
        Select Case strCompare
            Case "gt"
                If intHits > intCompareHit Then
                    bFound = True
                End If
            Case "eq"
                If intHits = intCompareHit Then
                    bFound = True
                End If
            Case "lt"
                If intHits < intCompareHit Then
                    bFound = True
                End If
            Case "ne"
                If intHits <> intCompareHit Then
                    bFound = True
                End If
        End Select
        Return bFound
    End Function

    Private Function GetArticleDistKode(ByRef xmlDoc As XmlDocument) As String
        Try
            Dim strDistKode As String = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBDistribusjonsKode']/@content").Value
            Return UCase(strDistKode)
        Catch
            Return "ZZZ"
        End Try
    End Function

    Private Function IsCustomerDistKode(ByRef rowCustomer As DataRow, ByVal strDisKode As String) As Boolean
        If strDisKode = "" Then Return True
        Try
            Dim CustDistKode As String = rowCustomer("Distkode") & ""
            If UCase(CustDistKode) = strDisKode Then
                Return True
            End If

            Dim intCustomerId As String = rowCustomer("CustomerId")
            Dim row As DataRow
            For Each row In dsCustomerDistKode.Tables(0).Select("CustomerId=" & intCustomerId)
                If strDisKode = UCase(row("DistKode")) Then Return True
            Next
        Catch
        End Try

        Return False
    End Function

    Private Function IsAvisDistKode(ByVal strDisKode As String) As Boolean
        Try
            If strDisKode = "" Then Return False
            Dim strTest = htDistKodeSatAviser(strDisKode) & ""
            If strTest <> "" Then
                Return True
            Else
                Return False
            End If
        Catch
        End Try

        Return False
    End Function

    Private Function CopyTrans(ByVal strFilename As String, ByRef rowCustomer As DataRow, Optional ByVal xmlDoc As XmlDocument = Nothing) As Boolean
        Dim customerId As Integer
        Dim delay As Integer
        Dim jobTypeID As Integer
        Dim jobDefID As Integer
        Dim strOutPath, strSuffix As String
        Dim strOutFile As String
        Dim tmpFile As String
        Dim row As DataRow
        Dim rowsJobs As Array
        Dim strErr As String
        Dim xsltFile As String
        Dim bEMail As Boolean
        Dim toEmail As String

        'Select all job rows for one customer
        customerId = rowCustomer("CustomerId")
        delay = rowCustomer("PushDelay")
        rowsJobs = dsCustomerJob.Tables(0).Select("CustomerId = " & customerId)

        For Each row In rowsJobs
            jobTypeID = row("JobTypeID")
            strOutPath = row("OutPath") & ""
            strSuffix = row("Suffix") & ""
            jobDefID = row("JobDefID")
            xsltFile = row("XsltFile") & ""
            bEMail = row("EMail")

            Dim strTempDoc As String = ""
            Dim bXsltWithParams As Boolean = False

            If strSuffix = "" Then
                strSuffix = Path.GetExtension(strFilename)
            Else
                strSuffix = "." & strSuffix
            End If

            If jobTypeID = 1 Then
                'Copy Files
                tmpFile = strFilename
            Else
                'Transform Files
                Dim XchgBeforeID As Integer = Val(row("XchgBeforeID") & "")
                Dim XchgAfterID As Integer = Val(row("XchgAfterID") & "")

                ' Hent temp-file i tempfile cache
                tmpFile = htTempFiles(jobDefID)

                ' Hvis fila ikke er konvertert allerede
                If tmpFile = "" Then
                    tmpFile = tempPath & "\JobDefID-" & jobDefID & "_" & Path.GetFileNameWithoutExtension(strFilename) & strSuffix

                    'Find and Replace before XSLT
                    If XchgBeforeID > 0 Then
                        strTempDoc = DoXchg(XchgBeforeID, strFilename, "")
                    End If

                    'XML Transformation, XSLT
                    If xsltFile <> "" Then
                        If strTempDoc <> "" Then
                            xmlDoc = New XmlDocument()
                            xmlDoc.PreserveWhitespace = True
                            xmlDoc.LoadXml(strTempDoc)
                        ElseIf xmlDoc Is Nothing Then
                            xmlDoc = New XmlDocument()
                            xmlDoc.PreserveWhitespace = True
                            xmlDoc.Load(strFilename)
                        End If
                        Try
                            strTempDoc = XsltProc.DoXslTransform(xmlDoc, jobDefID, customerId, row, bXsltWithParams)
                        Catch e As Exception
                            WriteErr(logFilePath, "Feil i Xslt", e)
                            strTempDoc = "<error><message>" & e.Message & "</message><stacktrace>" & e.StackTrace & "</stacktrace></error>"
                        End Try
                    End If

                    'Find and Replace after XSLT
                    If XchgAfterID > 0 Then
                        strTempDoc = DoXchg(XchgAfterID, strFilename, strTempDoc)
                    End If

                    ' Make temp file cache for all other customers who need this file
                    ' WriteFile(tmpFile, strTempDoc, False)
                    ' Ny kode sat inn av Roar Vestre og Avneet G. Singh 2005.07.05
                    Dim custEncoding As String
                    Try
                        custEncoding = row("Encoding") & ""
                    Catch ex As Exception
                        custEncoding = ""
                    End Try
                    If custEncoding = "" Then
                        ' Default Encoding=iso-8859-1
                        WriteFile(tmpFile, strTempDoc)
                    Else
                        WriteFile(tmpFile, strTempDoc, custEncoding)
                    End If

                    If Not bXsltWithParams Then
                        htTempFiles.Item(jobDefID) = tmpFile
                    End If
                Else
                        strTempDoc = ReadFile(tmpFile)
                End If
            End If

            'Kopier fil til Kundens mappe
            Try
                Dim running As Boolean = False
                Try
                    running = SMTPRunning()
                Catch
                End Try

                If bEMail And running Then
                    ' Sender fila som vedlegg i E-post
                    toEmail = row("SendToEmail")

                    Dim message As MailMessage
                    message = htMailCache(jobDefID)

                    If message Is Nothing Then
                        message = New MailMessage()
                        Try
                            message.Subject = "NTB " & _
                            xmlDoc.SelectSingleNode("/nitf/head/tobject/@tobject.type").InnerText & ": " & _
                            xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1").InnerText
                        Catch
                            message.Subject = "Nyhet fra NTB"
                        End Try
                        If strSuffix.ToLower().StartsWith(".htm") Then
                            MakeNewMessage(message, tmpFile, MailFormat.Html, strTempDoc)
                        ElseIf strSuffix.ToLower() = ".txt" Then
                            MakeNewMessage(message, tmpFile, MailFormat.Text, strTempDoc)
                        Else
                            MakeNewMessage(message, tmpFile)
                        End If
                        htMailCache(jobDefID) = message
                    End If

                    message.To = toEmail

                    SmtpMail.SmtpServer = smtpServer
                    SmtpMail.Send(message)
                    WriteLog(logFilePath, "Sendt p� Epost til: " & rowCustomer("CustomerName") & "/" & toEmail & " : " & Path.GetDirectoryName(tmpFile))
                ElseIf Not bEMail Then
                    'Copy file to destination
                    strOutFile = strOutPath & Path.DirectorySeparatorChar & Path.GetFileNameWithoutExtension(strFilename) & strSuffix

                    Dim msg As String = ""
                    If delay > 0 Then
                        strOutFile = strOutFile.Replace(strDefOutPath, strDelayPath)
                        msg = " ( Delay: " & delay & " )"
                    End If

                    File.Copy(tmpFile, strOutFile, True)

                    WriteLog(logFilePath, "Kopiert til: " & rowCustomer("CustomerName") & ": " & Path.GetDirectoryName(strOutFile) & msg)
                End If
            Catch e As Exception
                strErr = e.Message & e.StackTrace
                WriteErr(logFilePath, "Feil i filkopiering", e)
                Return False
            End Try

        Next
        Return True
    End Function

    Private Sub MakeNewMessage(ByRef message As MailMessage, ByVal tmpFile As String, Optional ByVal format As MailFormat = MailFormat.Text, Optional ByVal strHtmlDoc As String = "")
        message.From = "ntb@ntb.no"
        message.BodyEncoding = myEncoding
        message.BodyFormat = format

        If strHtmlDoc = "" Then
            message.Body = "Nyhet fra NTB, se vedlegg" & vbCrLf
            Dim attachment As MailAttachment = New MailAttachment(tmpFile)
            message.Attachments.Add(attachment)
        Else
            message.Body = strHtmlDoc
        End If

    End Sub

    Public Function WaitWhileBusy(ByVal SleepRetrySeconds As Integer, Optional ByVal TimeOutSeconds As Integer = 15) As Boolean
        Dim i As Integer
        For i = 0 To TimeOutSeconds / SleepRetrySeconds
            If bBusy Then
                Sleep(SleepRetrySeconds * 1000)
            Else
                Return True
            End If
        Next
        Return False
    End Function

    Private Function DoXchg(ByVal XchgID As Integer, ByRef tmpFile As String, ByVal strTempDoc As String) As String
        If strTempDoc = "" Then
            strTempDoc = ReadFile(tmpFile)
            If strTempDoc = "" Then
                Return ""
            End If
        End If

        Try
            Return XchgReplace.DoXchgReplace(XchgID, strTempDoc)
        Catch e As Exception
            WriteErr(logFilePath, "Feil i DoXchgReplace", e)
            Return strTempDoc
        End Try

    End Function

    'Checks folders for waiting files
    Private Sub WatchFolders()

        Dim row As DataRow
        For Each row In dsWatchFolder.Tables(0).Rows

            Dim id As Integer = 0
            Dim threshold As Integer = 0
            Dim email As String = ""
            Dim name As String = ""
            Dim cell As String = ""
            Dim path As String = ""
            Dim filter As String = ""

            Dim files As String()
            Dim f As String = ""
            Dim msg As String = ""

            id = row("CustomerId") & ""
            name = row("CustomerName") & ""
            threshold = row("Threshold") & ""
            path = row("OutPath") & ""
            filter = "*." & row("Suffix")
            email = row("ContactEmail") & ""
            cell = row("ContactMobile") & ""

            If filter = "*." Then filter &= "xml"

            'Check if error is sent within last hour
            Dim sendError As Boolean = True
            If htErrorStates.ContainsKey(path) Then
                sendError = Now.Subtract(htErrorStates(path)).Totalminutes > 60
            End If

            'Check if any old files is waiting
            If sendError Then
                sendError = False

                files = Directory.GetFiles(path, filter)
                For Each f In files
                    Dim dt As Date = File.GetLastWriteTime(f)

                    If Now.Subtract(dt).TotalMinutes > threshold Then
                        sendError = True
                    End If

                    msg &= f & vbCrLf
                Next

                'Send error
                If sendError Then

                    'Mail
                    msg = "Det har oppst�tt en feil i FTP-distribusjonen." & vbCrLf & vbCrLf & _
                        "Det ligger " & files.GetLength(0) & " filer i k� for overf�ring til " & name & vbCrLf & _
                        "i mappen '" & path & "'." & vbCrLf & vbCrLf & msg

                    Dim message As MailMessage = New MailMessage()
                    SmtpMail.SmtpServer = smtpServer
                    message.To = errorMailAddress & ";" & email
                    message.From = "505@ntb.no"
                    message.Subject = "Feil ved FTP-overf�ring til " & name
                    message.Body = msg

                    Try
                        SmtpMail.Send(message)
                    Catch
                        sendError = False
                    End Try

                    'SMS
                    Dim num As String
                    For Each num In errorSMSNumber.Split(";")
                        If num <> "" And sendError Then
                            msg = "[SMS]" & vbCrLf
                            msg &= "From=NTB-505" & vbCrLf
                            msg &= "To=" & num & vbCrLf
                            msg &= "Text=NTB Distribusjon: Det ligger " & files.GetLength(0) & " filer i k� for overf�ring til " & name
                            WriteFile(SMSFolder & "\SM" & num & id & ".GSM", msg)
                        End If
                    Next

                    For Each num In cell.Split(";")
                        If num <> "" And sendError Then
                            msg = "[SMS]" & vbCrLf
                            msg &= "From=NTB-505" & vbCrLf
                            msg &= "To=" & num & vbCrLf
                            msg &= "Text=NTB Distribusjon: Det ligger " & files.GetLength(0) & " filer i k� for overf�ring til " & name
                            WriteFile(SMSFolder & "\SM" & num & id & ".GSM", msg)
                        End If
                    Next

                    htErrorStates(path) = Now

                    WriteLogNoDate(logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
                    WriteLog(logFilePath, "Overv�kning - " & files.GetLength(0) & " filer i k� til " & name)
                    WriteLog(logFilePath, "Overv�kning - " & path)
                ElseIf htErrorStates.ContainsKey(path) Then
                    htErrorStates.Remove(path)
                End If
            End If
        Next

    End Sub

    Private Sub MoveDelayed()

        Dim logitem As String = ""
        Dim row As DataRow
        For Each row In dsDelayFolder.Tables(0).Rows

            Dim id As Integer = 0
            Dim delay As Integer = 0
            Dim name As String = ""
            Dim oPath As String = ""
            Dim dPath As String = ""
            Dim filter As String = ""

            Dim files As String()
            Dim f As String = ""
            Dim msg As String = ""

            id = row("CustomerId") & ""
            name = row("CustomerName") & ""
            delay = row("PushDelay") & ""
            oPath = row("OutPath") & ""
            dPath = oPath.Replace(strDefOutPath, strDelayPath)
            filter = "*." & row("Suffix")

            If filter = "*." Then filter &= "xml"

            files = Directory.GetFiles(dPath, filter)
            For Each f In files
                Dim dt As Date = File.GetLastWriteTime(f)

                If Now.Subtract(dt).TotalMinutes > delay Then
                    'Move file back to original folder
                    File.Move(f, oPath & "\" & Path.GetFileName(f))
                    logitem &= Format(Now, DATE_TIME) & ": Delay ( " & name & ", " & delay & " min. ) - " & Path.GetFileName(f) & " kopiert til kundemappe." & vbCrLf
                End If
            Next
        Next

        If logitem <> "" Then
            WriteLogNoDate(logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
            WriteLogNoDate(logFilePath, logitem.TrimEnd())
        End If
    End Sub

End Class
