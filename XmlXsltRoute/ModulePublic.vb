Imports System.Configuration.ConfigurationSettings
Imports System.Data.OleDb
Imports System.Text
Imports System.IO
Imports System.Web.Mail

Module ModulePublic
    Public Const TEXT_STARTED As String = "NTB_XmlXsltRoute Service Started"
    Public Const TEXT_STOPPED As String = "NTB_XmlXsltRoute Service Stopped"
    Public Const TEXT_INIT As String = "NTB_XmlXsltRoute Init finished"
    Public Const TEXT_PAUSE As String = "NTB_XmlXsltRoute Service Paused"
    Public Const TEXT_LINE As String = "----------------------------------------------------------------------------------"
    Public Const DATE_TIME As String = "yyyy-MM-dd HH:mm:ss"
    Public Const DATE_LOGFILE As String = "yyyy-MM-dd"

    Public myEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")
    Public txtEncoding As Encoding = Encoding.GetEncoding(1252)

    Public errFilePath As String = AppSettings("errFilePath")

    Public deleteTempFiles As Boolean = UCase(AppSettings("deleteTempFiles")) <> "NO"
	Public databaseOfflinePath As String = AppSettings("databaseOfflinePath")
	Public tempPath As String = AppSettings("tempPath")

    Public watchInitialDelay As Integer = AppSettings("watchInitialDelay") * 60 * 1000
    Public SMSFolder As String
    Public smtpServer As New String("")

    Public sqlConnectionString As String = AppSettings("SqlConnectionString")
    Public cn As New OleDbConnection(sqlConnectionString)

    Public Sub FillOneDataSet(ByRef dsDataSet As DataSet, ByVal strOfflineFile As String, ByVal strCommand As String, ByVal dataAdapter As OleDbDataAdapter, ByVal command As OleDbCommand, Optional ByVal strGroupListID As String = "")
        Dim strFile As String = databaseOfflinePath & "\" & strOfflineFile & strGroupListID & ".xml"
        If cn.State = ConnectionState.Open Then
            command.CommandText = strCommand
            Try
                dataAdapter.Fill(dsDataSet)
                dsDataSet.WriteXml(strFile, XmlWriteMode.WriteSchema)
            Catch e As Exception
                WriteErr(errFilePath, "Feil i FillOneDataSet: ", e)
                dsDataSet.ReadXml(strFile, XmlReadMode.ReadSchema)
            End Try
        Else
            dsDataSet.ReadXml(strFile,XmlReadMode.ReadSchema)
        End If
    End Sub

    Public Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String, ByRef e As Exception)
        Dim strLine As String

        On Error Resume Next

        strLine = "Error: " & Format(Now, DATE_TIME) & vbCrLf
        strLine &= strMessage & vbCrLf
        strLine &= e.Message & vbCrLf
        strLine &= e.Source & vbCrLf
        strLine &= e.StackTrace

        Dim strFile As String = strLogPath & "\Error-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.WriteLine(TEXT_LINE)
        w.Flush()  ' update underlying file
        w.Close()  ' close the writer and underlying file

        'If errorMailAddress <> "" Then
        '    SendErrMail(strLine)
        'End If
    End Sub

    Public Sub WriteLog(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim strLine As String

        strLine = Format(Now, DATE_TIME) & ": " & strMessage

        Dim w As StreamWriter = New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteLogNoDate(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strMessage)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteFile(ByRef strFileName As String, ByRef strContent As String)
        Dim w As New StreamWriter(strFileName, False, txtEncoding)
        w.WriteLine(strContent)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteFile(ByRef strFileName As String, ByRef strContent As String, ByVal encodingString As String)
        Dim custEncoding As Encoding
        Try
            custEncoding = Encoding.GetEncoding(encodingString)
        Catch ex As Exception
            custEncoding = txtEncoding
        End Try
        Dim w As New StreamWriter(strFileName, False, custEncoding)
        w.WriteLine(strContent)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Function ReadFile(ByVal strFileName As String) As String
        ' Simple File reader returns File as String
        Dim sr As New StreamReader(strFileName, txtEncoding)    ' File.OpenText("log.txt")
        ReadFile = sr.ReadToEnd
        sr.Close()
    End Function

    Public Sub MakePath(ByVal strPath As String)
        'If Not Directory.Exists(strPath) Then
        Directory.CreateDirectory(strPath)
        'End If
    End Sub

    Public Function MakeSubDirDate(ByRef strFilePath As String, ByVal dtTimeStamp As DateTime) As String
        Dim strFile As String = Path.GetFileNameWithoutExtension(strFilePath)
        Dim strPath As String = Path.GetDirectoryName(strFilePath)
        Dim strSub As String

        'strSub &= dtTimeStamp.Year & "\" & dtTimeStamp.Month & "-" & dtTimeStamp.Day
        strSub &= Format(dtTimeStamp, "yyyy-MM") & "\" & Format(dtTimeStamp, "yyyy-MM-dd")
        strPath = strPath & "\" & strSub & "\"
        If Not Directory.Exists(strPath) Then
            Directory.CreateDirectory(strPath)
        End If

        Return strPath & Path.GetFileName(strFilePath)

    End Function

    'Check if SMTP is running
    Function SMTPRunning() As Boolean

        If smtpServer = "" Then
#If Not Debug Then
            Dim computername As String = System.Environment.MachineName
            Dim objComp = GetObject("WinNT://" & computername & ",computer")
            Dim objService = objComp.GetObject("Service", "SMTPSVC")
            Return objService.Status = 4
#End If
        End If

        Return True
    End Function

    Sub SendErrMail(ByVal strMessage As String, ByVal address As String, Optional ByVal subject As String = "Error from NTB XmlRouteService")

        Dim message As New MailMessage

        SmtpMail.SmtpServer = smtpServer
        message.Subject = subject
        message.Body = strMessage
        message.From = address
        message.To = address
        message.BodyFormat = MailFormat.Text
        message.BodyEncoding = myEncoding
        SmtpMail.Send(message)

    End Sub

End Module
