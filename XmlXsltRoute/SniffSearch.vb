Imports System.Xml
Imports System.Text.RegularExpressions

Public Class SniffSearch

	Protected arrWords As String()
	Protected strDocText As String
	Protected htCustRegex As New Hashtable()

	Public Sub New()

	End Sub

	Public Sub SetXmlText(ByVal xmlDoc As XmlDocument)
		Dim bodyNodes As XmlNodeList
		Dim node As XmlNode
        bodyNodes = xmlDoc.SelectNodes("//du-key/@key | //p | //hl1 | //h2")

		strDocText = vbCrLf
		For Each node In bodyNodes
			If node.InnerText <> "" Then
				strDocText &= node.InnerText & vbCrLf
			End If
		Next

	End Sub

	Public Sub SetRegexCustomer(ByVal CustomerId As Integer, ByVal strSearch As String, ByVal logFilePath As String)
		strSearch = strSearch.Replace(" AND ", "+")

		Dim arrSearch As String() = strSearch.Split("+")
		Dim collRegex As New Collection()

        For Each strSearch In arrSearch

            Dim fnutt As Boolean = False
            Dim prevFnutt As Boolean = False

            strSearch = strSearch.Replace("'", "").Trim()
            strSearch = Regex.Replace(strSearch, "\s{2,}", " ")

            Dim i As Integer = 0
            While i < strSearch.Length

                If strSearch.Chars(i) = """" And Not fnutt Then
                    fnutt = True
                ElseIf strSearch.Chars(i) = """" And fnutt Then
                    fnutt = False
                    prevFnutt = True
                End If

                If strSearch.Chars(i) = " " And fnutt Then
                    strSearch = strSearch.Substring(0, i) & "�" & strSearch.Substring(i + 1)
                End If

                i += 1
            End While

            Try
                strSearch = "(\W" & strSearch.Replace(" ", "\W|\W") & "\W)"
                strSearch = strSearch.Replace("*\W", "")
                strSearch = strSearch.Replace("\W*", "")
                strSearch = strSearch.Replace("-", "\W")

                strSearch = strSearch.Replace("""", "")
                strSearch = strSearch.Replace("�", " ")

                Dim r As New Regex(strSearch, RegexOptions.Multiline Or RegexOptions.IgnoreCase Or RegexOptions.Compiled)
                collRegex.Add(r)

            Catch e As Exception
                WriteErr(logFilePath, "ERROR in Regex: " & strSearch & ", Customer: " & CustomerId, e)
            End Try

        Next

        htCustRegex.Add(CustomerId, collRegex)

	End Sub

    'Does the sniff search and returns:

    '1 - Sniff matched the text
    '0 - No match
    '-1 - No sniffsearch defined
    Public Function Find(ByVal customerID As Integer) As Integer
        Dim r As Regex
        Dim m As Match
        Dim collRegex As Collection
        Dim bFound As Boolean

        collRegex = htCustRegex(customerID)
        If collRegex Is Nothing Then
            'Return False
            Return -1
        End If

        For Each r In collRegex
            m = r.Match(strDocText)
            bFound = m.Success

            If Not bFound Then Return 0
        Next

        Return 1
    End Function

    Public Function TestFindRegex() As Boolean
        Dim r As Regex
        Dim m As Match
        Dim strSearch As String

        Do
            If strSearch = "" Then GoTo Skip
            strSearch = strSearch.Replace(" AND ", "+")

            Dim arrSearch As String() = strSearch.Split("+")
            Dim bFound As Boolean
            Dim collRegex As New Collection()

            For Each strSearch In arrSearch
                strSearch = Trim(strSearch)
                strSearch = "(\W" & strSearch.Replace(" ", "\W|\W") & "\W)"
                strSearch = strSearch.Replace("*\W", "")
                strSearch = strSearch.Replace("\W*", "")
                strSearch = strSearch.Replace("-", "\W")

                r = New Regex(strSearch, RegexOptions.Multiline Or RegexOptions.IgnoreCase Or RegexOptions.Compiled)
                collRegex.Add(r)

                Console.WriteLine("S�ker etter: " & strSearch)
            Next

            r = Nothing

            For Each r In collRegex
                Try

                    m = r.Match(strDocText)
                    bFound = m.Success

                    While m.Success
                        Console.WriteLine("Found: " & m.Groups(1).Value _
                        & " at " & m.Groups(1).Index.ToString())

                        m = m.NextMatch()
                    End While

                    If Not bFound Then
                        'Return False
                        Exit For
                    End If

                Catch e As Exception
                    Console.WriteLine("ERROR: " & e.Message)
                End Try
            Next

            Console.WriteLine(bFound)
            Console.WriteLine()

Skip:
            Console.Write("Oppgi s�ket: ")
            strSearch = Console.ReadLine()
            Console.WriteLine()

            If strSearch = "t" Then
                Console.WriteLine(strDocText)
                strSearch = ""
            End If
        Loop While strSearch <> "q"
        End
    End Function

End Class
