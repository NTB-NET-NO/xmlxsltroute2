<?xml version="1.0" encoding="iso-8859-1" standalone="yes" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" indent="yes"/>

<xsl:template match="nitf">
<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">

	<xsl:copy-of select="head"/>

	<body>
	<xsl:apply-templates select="body/body.head" />
	<body.content />
	<xsl:copy-of select="body/body.end"/>
	</body>
	
</nitf>

</xsl:template>

<xsl:template match="body.head">
		<hedline>
			<hl1>
			<xsl:text>NTB: </xsl:text>
			<xsl:value-of select="hedline/hl1" />
			</hl1>
		</hedline>
		<xsl:copy-of select="distributor"/>
</xsl:template>

</xsl:stylesheet>