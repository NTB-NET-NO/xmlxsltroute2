<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>

<xsl:template match="/message">
<xsl:text disable-output-escaping="yes">From: "NTB" &lt;rov@ntb.no>
To: &lt;rov@ntb.no>, &lt;roarvest@online.no>
Subject: Ny lydfil fra NTB
Date: Mon, 6 Aug 2001 15:43:47 +0200
MIME-Version: 1.0
Content-Type: multipart/mixed;
	boundary="----=_NextPart_000_0009_01C11E8E.94DA2C40"
X-Priority: 3
X-MSMail-Priority: Normal
X-Mailer: Microsoft Outlook Express 5.50.4133.2400
X-MimeOLE: Produced By Microsoft MimeOLE V5.50.4133.2400

This is a multi-part message in MIME format.

------=_NextPart_000_0009_01C11E8E.94DA2C40
Content-Type: text/plain;
	charset="iso-8859-1"
Content-Transfer-Encoding: quoted-printable

</xsl:text>
Lydfilen: &quot;<xsl:value-of select="sfile"/>&quot;, er oversendt med ftp

For nyhetsmeldingen:<xsl:value-of select="text"/>
<xsl:text>
------=_NextPart_000_0009_01C11E8E.94DA2C40--
</xsl:text>
</xsl:template>



</xsl:stylesheet>
 

 