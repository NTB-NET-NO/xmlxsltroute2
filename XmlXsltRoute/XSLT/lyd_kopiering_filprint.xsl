<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:user="http://mycompany.com/mynamespace"
	version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="yes" indent="yes"/>

<!-- 
	Styleshet for overføring av lydfiler fra Filprint til Ut-mappe for FTP til SoundOnWeb
	Sist endret Av Roar Vestre 31.10.2002
-->

<msxsl:script language="VBScript" implements-prefix="user">
	Function CopyFile(nodelist)
		dim strFileName, strErrMsg, strOKMsg
		dim oFile, CR
		CR = vbCrLf
		strOkMsg = "From:505@ntb.no" + CR + "To:pt@ntb.no" + CR + "Subject: Overføring av lydfil til Saxotech"
		strErrMsg = "From:505@ntb.no" + CR + "To:pt@ntb.no,505@ntb.no" + CR + "Subject: Overføring av lydfil til Saxotech Feilet "
	
		on error resume next
		strFileName = nodelist.nextNode().text

		if Err.Number &lt;> 0 then
			CopyFile = strErrMsg + CR + "Feil: " + err.Description
			exit Function
		end if

		strFileName = replace(strFileName, "\\Filprint\NTB_DirekteRed\", "Z:\")
		Set oFile = CreateObject("Scripting.FileSystemObject")
	
		oFile.copyfile strFileName, "D:\TransForm\spool\soundmp3\"
		if Err.Number = 0 then
			CopyFile = strOkMsg
		else
			CopyFile = strErrMsg + CR + "Feil: " + err.Description
		end if

		on error goto 0
		set oFile = nothing
	End Function
</msxsl:script>


<xsl:template match="/nitf">
<xsl:value-of select="user:CopyFile(head/meta[@name='ntb-lydfil']/@content)"/>&#32;
<xsl:value-of select="head/meta[@name='ntb-date']/@content"/>&#32;
<xsl:value-of select="body/body.head/hedline/hl1"/>&#32;
<xsl:value-of select="head/meta[@name='ntb-lydfil']/@content"/>&#32;
<xsl:apply-templates select="head/meta[@name='ntb-lyd']/@content"/>
</xsl:template>

<xsl:template match="head/meta[@name='ntb-lyd']/@content">
Windows media - http://194.19.39.29/kunde/ntb/asx/<xsl:value-of select="."/>.asx
<xsl:text>
&#160;
</xsl:text>
HTML kode for lyden:

<script language="javascript" src="http://194.19.39.29/kunde/ntb/flashsound.js">
</script>
<script language="javascript">
	var lyd = new FlashSound();
</script>
<xsl:text>
&#160; 
</xsl:text>
<a>
	<xsl:attribute name="href">http://194.19.39.29/kunde/ntb/asx/<xsl:value-of select="."/>.asx</xsl:attribute>
	<xsl:attribute name="onmouseover">lyd.TGotoAndPlay('/','start')</xsl:attribute>
	<xsl:attribute name="onmouseout">lyd.TGotoAndPlay('/','stop')</xsl:attribute>
	<img src="http://194.19.39.29/kunde/ntb/grafikk/ntb.gif" border="0"/>
</a>

<xsl:text>
&#160;
</xsl:text>
<script>
	lyd.embedSWF("http://194.19.39.29/kunde/ntb/flash/<xsl:value-of select="."/>.swf");
</script>


</xsl:template>

</xsl:stylesheet>