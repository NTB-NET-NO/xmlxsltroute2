<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="text" encoding="iso-8859-1" omit-xml-declaration="yes" standalone="yes"/>

<!-- 
	XSLT Stylesheet to transform to XTG format by Jon Henning Bergane
-->

<xsl:template match="/nitf">
<xsl:text>&lt;e1&gt;</xsl:text>
@tid:<xsl:value-of select="substring(head/pubdata/@date.publication, 7, 2)"/>.<xsl:value-of select="substring(head/pubdata/@date.publication, 5, 2)"/>.<xsl:value-of select="substring(head/pubdata/@date.publication, 3, 2)"/>&#160;<xsl:value-of select="substring(head/pubdata/@date.publication, 10, 2)"/>:<xsl:value-of select="substring(head/pubdata/@date.publication, 12, 2)"/>:<xsl:value-of select="substring(head/pubdata/@date.publication, 14, 2)"/>
@kat1:<xsl:value-of select="head/tobject/@tobject.type"/>
@kat2:
@pri:<xsl:value-of select="head/docdata/urgency/@ed-urg"/>
@sign:<xsl:value-of select="head/revision-history/@name"/>
@stikkord:<xsl:value-of select="head/docdata/du-key/@key"/>
@tittel:<xsl:value-of select="body/body.head/hedline/hl1"/>
@red:<xsl:apply-templates select="head/docdata/ed-msg"/>
@byline:<xsl:apply-templates select="body/body.head/byline"/>

<xsl:apply-templates select="body/body.content"/>
<xsl:apply-templates select="body/body.content/media[@media-type='audio']"/>
<xsl:if test="body/body.content/media[@media-type='image' and not(@class)]">
<xsl:apply-templates select="body/body.content/media[@media-type='image' and not(@class)]"/>
</xsl:if>
@tagline:<xsl:value-of select="body/body.end/tagline"/>

<xsl:if test="body/body.content/media[@class='prm']">
<!-- Vedlegg for PRM-meldinger fra NTB+ -->
<hr />
@vedlegg:Vedlegg:
<table>
<xsl:apply-templates select="body/body.content/media[@class='prm']"/>
</table>
</xsl:if>

</xsl:template>


<!-- Templates -->

<xsl:template match="head/docdata/ed-msg">
<xsl:if test="normalize-space(@info)!=''">Til red: <xsl:value-of select="@info"/>
</xsl:if>
</xsl:template>

<xsl:template match="body/body.content">
<xsl:choose>
<xsl:when test="p">
<xsl:apply-templates select="p | hl2 | table | br"/>
</xsl:when>	
<xsl:otherwise>
<xsl:value-of select="."/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="body/body.head/byline">
<xsl:value-of select="."/>

</xsl:template>

<xsl:template match="p[.!='']">
<!-- Normal paragraphs -->
@txt:<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="p[.='']">
<!-- Empty paragraphs -->
	
</xsl:template>

<xsl:template match="p[@lede='true' and . !='']">
<!-- Paragraph of "ingress" -->
@ingress:<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="p[@innrykk='true']">
<!-- Paragraph of "Br�dtekst innrykk" -->
@txt:<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="p[@class='table-code']">
<!-- Paragraph of "tabellkode"
@txt:<xsl:value-of select="."/>
 -->
</xsl:template>

<!-- Added for handeling some messages from Fretex with <br> as paragraph tags -->
<xsl:template match="br">
<!-- Normal paragraphs -->
@txt:<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="hl2">
<!-- Mellomtittel -->
@mtit:<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="table">
<!-- Tabeller -->
@txt:
<xsl:text>
</xsl:text>

<xsl:apply-templates select="tr"/>

<xsl:text>
</xsl:text>

</xsl:template>

<xsl:template match="tr">
<xsl:apply-templates select="td"/>
<xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="td">
<xsl:value-of select="."/>
<xsl:text>	</xsl:text>
</xsl:template>

<xsl:template match="body/body.content/media[@media-type='image' and not(@class)]">
<!-- Template for Scanpix media	-->
@bilde:<xsl:value-of select="media-reference/@source"/>
@bildetekst:<xsl:value-of select="media-caption"/>
</xsl:template>

<xsl:template match="body/body.content/media[@class='prm']">
<!-- Template for PRM media	-->
<tr><td class="viewNews"><a target="_blank">
<xsl:attribute name="href">http://193.75.33.34/prm_vedlegg/vedlegg/<xsl:value-of select="media-reference/@alternate-text"/>/<xsl:value-of select="media-reference/@source"/></xsl:attribute>
<xsl:value-of select="media-reference/@name"/></a>
</td><td class="viewNews"><xsl:value-of select="media-caption"/></td></tr>
</xsl:template>

<xsl:template match="body/body.content/media[@media-type='audio']">
<!--
	Hardcoded paths for Image and JavaScript (complete URL is included in NTB's NITF)
-->
<script language="javascript" src="http://194.19.39.29/kunde/ntb/flashsound.js"></script>
<script language="javascript">var lyd = new FlashSound();</script>
	<a href="javascript://">
		<xsl:attribute name="onmouseover">lyd.TGotoAndPlay('/','start')</xsl:attribute>
		<xsl:attribute name="onmouseout">lyd.TGotoAndPlay('/','stop')</xsl:attribute>
		<img src="../images/sound_large.gif" border="0" align="right"/>
	</a>
<script>lyd.embedSWF("<xsl:value-of select="media-reference[@mime-type='application/x-shockwave-flash']/@source"/>");</script>
</xsl:template>

</xsl:stylesheet>