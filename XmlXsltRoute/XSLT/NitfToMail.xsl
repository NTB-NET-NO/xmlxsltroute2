<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

<xsl:param name="SendToEmail" select="'505@ntb.no'"/>

<xsl:template match="/">
<xsl:text disable-output-escaping="yes"><![CDATA[From: rov@ntb.no
To: ]]></xsl:text>
<xsl:value-of select="$SendToEmail"/>
<xsl:text disable-output-escaping="yes"><![CDATA[
Subject: Ny lydfil fra NTB
Date: Mon, 6 Aug 2001 15:43:47 +0200
MIME-Version: 1.0
Content-Type: multipart/mixed;
	boundary="----=_NextPart_000_0009_01C11E8E.94DA2C40"
X-Priority: 3
X-MSMail-Priority: Normal
X-Mailer: Microsoft Outlook Express 5.50.4133.2400
X-MimeOLE: Produced By Microsoft MimeOLE V5.50.4133.2400

This is a multi-part message in MIME format.

------=_NextPart_000_0009_01C11E8E.94DA2C40
Content-Type: text/plain;
	charset="iso-8859-1"
Content-Transfer-Encoding: quoted-printable
]]></xsl:text>
Nyhetsmelding i NITF XML-format: <xsl:value-of select="//hl1"/>
<xsl:text>
------=_NextPart_000_0009_01C11E8E.94DA2C40
Content-Type: text/xml;
	name="ntb2116279.xml"
Content-Transfer-Encoding: quoted-printable
Content-Disposition: attachment;
	filename="ntb2116279.xml"

</xsl:text>
<xsl:copy-of select="."/>
<xsl:text>
------=_NextPart_000_0009_01C11E8E.94DA2C40--
</xsl:text>
</xsl:template>



</xsl:stylesheet>
 

 