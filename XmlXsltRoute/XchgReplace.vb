Imports System.Data.OleDb
Imports System.Configuration.ConfigurationSettings
Imports System.Text

Public Class XchgReplace

    Public Structure recReplace
        Dim xchgType As Integer
        Dim find1 As String
        Dim find2 As String
        Dim replace1 As String
        Dim replace2 As String
        Dim start As Integer
        Dim count As Integer
    End Structure

    Public Structure recIndex
        Dim first As Integer
        Dim last As Integer
    End Structure

    Public Enum typeXchgType
        Replace = 1
        BulkReplace = 2
        PairReplace = 3
        HeadFoot = 4
        ReplaceExtended = 5
    End Enum

    Private Shared dsXchg As DataSet

    Private Shared htXchg As Hashtable = New Hashtable()
    Private Shared arrXchg() As recReplace '= New recReplace()

    Public Shared Sub Init(ByVal cn As OleDbConnection)
        FillDataSets(cn)
        FillArray()
    End Sub

    Public Shared Function DoXchgReplace(ByRef XchgId As Integer, ByVal strDoc As String) As String
        Dim sbDoc As New StringBuilder()
        Dim row As Data.DataRow
        Dim i As Integer
        Dim ReplaceRecord As recReplace
        Dim index As recIndex = htXchg(XchgId)

        sbDoc.Append(strDoc)

        For i = index.first To index.last
            ReplaceRecord = arrXchg(i)
            With ReplaceRecord
                Select Case .xchgType
                    Case typeXchgType.Replace
                        sbDoc.Replace(.find1, .replace1)

                    Case typeXchgType.ReplaceExtended
                        ' Find2 and Replace2 is start and end strings to replace in between
                        ReplaceExtended(sbDoc, .find1, .replace1, .find2, .replace2, .start, .count)

                    Case typeXchgType.PairReplace
                        PairReplace(sbDoc, .find1, .find2, .replace1, .replace2, .start, .count)

                    Case typeXchgType.BulkReplace
                        BulkReplace(sbDoc, .find1, .find2, .replace1, .start, .count)

                    Case typeXchgType.HeadFoot
                        sbDoc.Insert(0, .replace1)
                        sbDoc.Append(.replace2)
                End Select
            End With
        Next

        Return sbDoc.ToString

    End Function

    Private Shared Sub FillArray()
        Dim row As DataRow
        Dim i, i0 As Integer
        Dim XchgId As Integer
        Dim XchgIdPrev As Integer
        Dim index As recIndex
        Dim bNewXchg As Boolean
        Dim strTemp As String

        For Each row In dsXchg.Tables(0).Rows
            XchgId = row("XchgId")
            bNewXchg = XchgIdPrev <> XchgId

            If bNewXchg And i > 0 Then
                ' Add Xchg Record Start and stop point in Hashtable
                index.first = i0
                index.last = i - 1
                htXchg.Add(XchgIdPrev, index)
                i0 = i
            End If

            XchgIdPrev = XchgId

            ReDim Preserve arrXchg(i)

            With arrXchg(i)
                .xchgType = row("xchgTypeID")
                .find1 = row("find1") & ""
                .find2 = row("find2") & ""
                .replace1 = row("replace1") & ""
                .replace2 = row("replace2") & ""
                .start = row("start")
                .count = row("count")
                If row("Find1Entity") Then
                    strTemp = ConvXmlEntities(.find1)
                    .find1 = strTemp
                End If
                If row("Find2Entity") Then
                    strTemp = ConvXmlEntities(.find2)
                    .find2 = strTemp
                End If
                If row("Replace1Entity") Then
                    strTemp = ConvXmlEntities(.replace1)
                    .replace1 = strTemp
                End If
                If row("Replace2Entity") Then
                    strTemp = ConvXmlEntities(.replace2)
                    .replace2 = strTemp
                End If
            End With

            i += 1
        Next

        ' Add Last Xchg Record Start and stop point in Hashtable
        index.first = i0
        index.last = i - 1
        htXchg.Add(XchgId, index)

    End Sub

    Private Shared Sub FillDataSets(ByVal cn As OleDbConnection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand = New OleDbCommand()
        dataAdapter.SelectCommand = command
        command.Connection = cn
        dsXchg = New DataSet()
        FillOneDataSet(dsXchg, "dsXchg", "SELECT * FROM GetXchg", dataAdapter, command)
    End Sub

    'Replaces everything found. StringBuilder as input!
    Private Shared Sub ReplaceExtended(ByRef sbDoc As StringBuilder, ByVal strFind As String, ByVal strReplace As String, ByVal strFindStart As String, ByVal strFindEnd As String, Optional ByVal intStart As Integer = 0, Optional ByVal intCount As Integer = -1)
        'Replaces everything found!

        If strFind = "" Then Return

        Dim i As Integer
        Dim intLastStart As Integer = 0
        Dim intEnd As Integer = 0
        Dim strDocEnd As String
        Dim intLenFind As Integer = Len(strFind)

        Dim sbTemp As StringBuilder = New StringBuilder()
        Dim strDoc As String = sbDoc.ToString

        If strFindStart <> "" Then
            Dim intFindStart As Integer = strDoc.IndexOf(strFindStart)
            If intFindStart > -1 Then
                intStart = intFindStart + intStart
            Else
                Exit Sub
            End If
        End If

        If strFindEnd <> "" Then
            Dim intFindEnd As Integer = strDoc.IndexOf(strFindEnd, intStart + intLenFind)
            If intFindEnd > -1 Then
                strDocEnd = strDoc.Substring(intFindEnd)
                strDoc = strDoc.Substring(0, intFindEnd)
            Else
                Exit Sub
            End If
        End If

        ' Allow max 10000 susbstitutions, if set to -1
        If intCount < 0 Then
            intCount = 10000
        End If

        intLastStart = 0

        For i = 1 To intCount
            intStart = strDoc.IndexOf(strFind, intStart)
            If intStart = -1 Then Exit For

            sbTemp.Append(strDoc.Substring(intLastStart, intStart - intLastStart) & strReplace)

            intStart = intStart + intLenFind
            intLastStart = intStart
        Next

        If intLastStart = 0 Then
            'No string found, do nothing
            Return
        Else
            'Add last part of string
            sbTemp.Append(strDoc.Substring(intLastStart))
            sbTemp.Append(strDocEnd)
            'Dim strDocResult As String = sbTemp.ToString
            sbDoc = sbTemp
            Return
        End If
    End Sub


    'Replaces everything between two tags. StringBuilder as input!
    Private Shared Sub BulkReplace(ByRef sbDoc As StringBuilder, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, Optional ByVal intStart As Integer = 0, Optional ByVal intCount As Integer = -1)
        'Replaces everything between two tags!
        If strFind = "" Or strFind2 = "" Then Return

        Dim i As Integer
        Dim intLastStart As Integer = 0
        Dim intEnd As Integer = 0

        Dim intLenFind As Integer = Len(strFind)
        Dim intLenFind2 As Integer = Len(strFind2)

        Dim sbTemp As StringBuilder = New StringBuilder()
        Dim strDoc As String = sbDoc.ToString

        ' Allow max 10000 susbstitutions, if set to -1
        If intCount < 0 Then
            intCount = 10000
        End If

        intLastStart = 0

        For i = 1 To intCount
            intStart = strDoc.IndexOf(strFind, intStart)
            If intStart = -1 Then Exit For

            intEnd = strDoc.IndexOf(strFind2, intStart + intLenFind)
            If intEnd = -1 Then Exit For

            sbTemp.Append(strDoc.Substring(intLastStart, intStart - intLastStart) & strReplace)

            intStart = intEnd + intLenFind2
            intLastStart = intStart
        Next

        If intLastStart = 0 Then
            'No string pair found, do nothing
            Return
        Else
            'Add last part of string
            sbTemp.Append(strDoc.Substring(intLastStart))
            sbDoc = sbTemp
            Return
        End If
    End Sub

    ' Replaces all paired start and end tags!
    Private Shared Sub PairReplace(ByRef sbDoc As StringBuilder, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, ByVal strReplace2 As String, Optional ByVal intStart As Integer = 0, Optional ByVal intCount As Integer = -1)

        If strFind = "" Or strFind2 = "" Then Return

        Dim i As Integer
        Dim intLastStart As Integer = 0
        Dim intEnd As Integer = 0
        Dim intLenFind As Integer = Len(strFind)
        Dim intLenFind2 As Integer = Len(strFind2)

        Dim sbTemp As StringBuilder = New StringBuilder()
        Dim strDoc As String = sbDoc.ToString

        ' Allow max 10000 susbstitutions, if set to -1
        If intCount < 0 Then
            intCount = 100000
        End If

        For i = 1 To intCount
            intStart = strDoc.IndexOf(strFind, intStart)
            If intStart = -1 Then Exit For

            intEnd = strDoc.IndexOf(strFind2, intStart + intLenFind)
            If intEnd = -1 Then Exit For

            sbTemp.Append(strDoc.Substring(intLastStart, intStart - intLastStart) & strReplace & strDoc.Substring(intStart + intLenFind, intEnd - intStart - intLenFind) & strReplace2)

            intStart = intEnd + intLenFind2
            intLastStart = intStart
        Next

        If intLastStart = 0 Then
            'No string pair found, do nothing
            Return
        Else
            'Add last part of string
            sbTemp.Append(strDoc.Substring(intLastStart))
            sbDoc = sbTemp
            Return
        End If
    End Sub

    Private Shared Function GetStringPortion(ByRef strString As String, ByVal strStartTag As String, ByVal strEndTag As String) As String
        ' Find Portion of a text-string found between two tags (substrings)
        ' If not both tags are found the function returns an empty string

        Dim intStart, intEnd As Integer

        intStart = strString.IndexOf(strStartTag) + strStartTag.Length
        If intStart = -1 Then
            Return ""
        End If

        intEnd = strString.IndexOf(strEndTag, intStart)
        If intEnd = -1 Then
            Return ""
        End If

        Return strString.Substring(intStart, intEnd - (intStart))
    End Function

    Private Shared Function ConvXmlEntities(ByVal strXmlString As String) As String
        Dim intStart As Integer
        Dim intSlutt As Integer
        Dim strChar As String
        Dim intChar As String
        Dim strTemp As String
        Dim strStart As String
        Dim strEnd As String

        strXmlString = strXmlString.Replace("&lt;", "<")
        strXmlString = strXmlString.Replace("&gt;", ">")
        strXmlString = strXmlString.Replace("&apos;", "'")
        strXmlString = strXmlString.Replace("&quot;", """")

        intStart = 0
        intStart = strXmlString.IndexOf("&", intStart)

        Do Until intStart = -1
            intSlutt = strXmlString.IndexOf(";", intStart)
            If intSlutt = -1 Then Exit Do
            strChar = strXmlString.Substring(intStart + 1, intSlutt - intStart)
            If strChar.StartsWith("#") Then
                If strChar = "#" Then
                    strChar = ""
                Else
                    intChar = Val(strChar.Substring(1))
                    strChar = Chr(intChar) & ""
                End If
                strStart = strXmlString.Substring(0, intStart)
                strEnd = strXmlString.Substring(intSlutt + 1)
                strXmlString = strStart & strChar & strEnd
            End If
            intStart = strXmlString.IndexOf("&", intStart + 1)
        Loop

        strXmlString = strXmlString.Replace("&amp;", "&")
        Return strXmlString

    End Function

End Class
